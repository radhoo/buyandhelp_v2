<!doctype html>
<!--[if IE 9]> <html class="no-js ie9" lang="en"> <![endif]-->
<!--[if gt IE 9]><!--> <html class="no-js " lang="en"> <!--<![endif]-->
	<head>
		<!-- basic page needs -->	
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title>BuyAndHelp</title>
        <meta name="description" content="add your site description here">
		<!-- mobile meta -->
        <meta name="viewport" content="width=device-width, initial-scale=1">
		<!-- favicon -->
		<link rel="shortcut icon" type="image/x-icon" href="img/favicon.ico">
		<!-- fancybox -->
		<link rel="stylesheet" href="css/jquery.fancybox.css">
		<!-- mobile menu -->
		<link rel="stylesheet" href="css/meanmenu.min.css">		
		<!-- jquery-ui-slider -->
		<link rel="stylesheet" href="css/jquery-ui-slider.css">		
		<!-- nivo-slider css -->
		<link rel="stylesheet" href="css/nivo-slider.css">
		<!-- owl.carousel css -->
		<link rel="stylesheet" href="css/owl.transitions.css">
		<link rel="stylesheet" href="css/owl.theme.css">
		<link rel="stylesheet" href="css/owl.carousel.css">
		<!-- animate css -->
		<link rel="stylesheet" href="css/animate.css">
		<!-- fonts -->
		<link href='http://fonts.googleapis.com/css?family=Poppins:200,300,400,500,600,700,800,900' rel='stylesheet' type='text/css' />
		<!-- font-awesome css -->
		<link rel="stylesheet" href="css/font-awesome.min.css">
		<!-- bootstrap css -->
		<link rel="stylesheet" href="css/bootstrap.min.css">
		<!-- style css -->
		<link rel="stylesheet" href="style.css">
		<link rel="stylesheet" href="custom.css">
		<!-- responsive css -->
		<link rel="stylesheet" href="css/responsive.css">
		<!-- modernizr js -->
		<script src="js/vendor/modernizr-2.6.2.min.js"></script>
		<!--[if lt IE 9]>
		  <script src="js/vendor/html5shiv.min.js"></script>
		  <script src="js/vendor/respond.min.js"></script>
		<![endif]-->		
	</head>
	<body class="index-4">
		<!--[if lt IE 8]>
			<p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
		<![endif]-->
		
		<!-- Add your site or application content here -->
		<!-- header-area start -->
		<?php include('assets/header.php'); ?>
		<!-- header-area end -->
		<!-- main content area start  -->
		<section class="main-content-area">
			<div class="container">		
				<!-- about-me start -->
				<div class="row about-me">
					<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
						<div class="about-greentech-text">
							<h1>Intrebari <strong>Frecvente</strong></h1>
							<h3>Ultimul update: 5 Oct 2017</h3>
							<p>&nbsp;</p>
							<p><strong>Cum functioneaza BuyAndHelp.ro ?</strong></p>
							<p>Sa presupunem ca doresti ca cumperi o pereche noua de pantofi de pe websitul Y. Intri pe BuyAndHelp.ro, dai click pe logo-ul website-ului Y si cumperi acei pantofi. In urma acestei achizitii, BuyAndHelp.ro primeste un comision din partea website-ului Y pe care il va dona cauzei sociale alese de tine.</p>
							<div class="clearfix">&nbsp;</div>
							
							<p><strong>Voi plati mai mult cumparand prin intermediul BuyAndHelp ?</strong></p>
							<p>Nu. Pretul produselor este cel afisat pe website-ul magazinului de unde doresti sa cumperi. Nu exista nicio taxa aditionala.</p>
							<div class="clearfix">&nbsp;</div>
							
							<p><strong>De unde provin cazurile sociale ?</strong></p>
							<p>Toate cazurile sociale prezentate pe acest website au fost adaugate de catre echipa BuyAndHelp.ro, asigurandu-ne astfel ca ele sunt reale si persoanele in cauza au nevoie cu adevarat de sprijin financiar.</p>
							<div class="clearfix">&nbsp;</div>
							
							<p><strong>Pot trimite si eu un caz social pentru a fi inclus pe BuyAndHelp.ro ?</strong></p>
							<p>Avem in plan dezvoltarea websitului nostru pentru a permite utilizatorilor sa adauge cazuri sociale.</p>
							<div class="clearfix">&nbsp;</div>
							
							<p><strong>Ce este marketingul afiliat ?</strong></p>
							<p>Marketingul afiliat este o forma de marketing bazata pe plata in functie de rezultate. Promovarea unui magazin online se face gratuit, afiliatii fiind platiti doar in cazul vanzarilor cu un procent din valoarea acestora.</p>
							<div class="clearfix">&nbsp;</div>
							
							<p><strong>Cum sunt realizate parteneriatele cu magazinele online ?</strong></p>
							<p>Parteneriatele cu toate magazinele online prezente in lista BuyAndHelp.ro sunt intermediate de <a href="http://www.2parale.ro" target="_blank">SC 2Parale Afiliere SRL</a>, una dintre cele mai mari si importante retele de afiliere din Romania.</p>
							<div class="clearfix">&nbsp;</div>
							
							<p><strong>Ce este fondul BuyAndHelp ?</strong></p>
							<p>Persoanele care nu selecteaza o cauza din lista de ONG-uri atunci cand cumpara ceva trimit banii in Fondul BuyAndHelp. Toti acesti bani sunt folositi in campaniile proprii ale platformei. <a href="https://www.facebook.com/buyandhelp/posts/1943843625849472" target="_blank"/>Prima campanie de acest tip</a> a fost realizata pentru "Centrul de ingrijire si asistenta Cluj-Napoca", cumparandu-le cateva saltele anti-escare.</p>
							<div class="clearfix">&nbsp;</div>
							
							<p><strong>De ce apare doar 1 singur produs in lista de donatii ?</strong></p>
							<p>In momentul in care cumperi mai multe produse, noi afisam doar primul din lista ta de cumparaturi. Asta e din motive legate de spatiu si design. <br/>
							Ex. Daca vei cumpara un monitor si o tastatura, noi afisam pe homepage / pagina ONG-ului doar monitorul. Suma donata cuprinde toate produsele, doar descrierea e limitata la primul produs.</p>
							<div class="clearfix">&nbsp;</div>
							
							<p><strong>Ce ce nu apare donatia mea ?</strong></p>
							<p>Donatiile apar in lista de pe homepage / pagina ONG-ului doar dupa ce acestea sunt aprobate de magazin (plata a fost facuta, produsul a fost livrat si nu a fost returnat). Acest proces poate dura pana la 30 de zile.</p>
							<div class="clearfix">&nbsp;</div>
							
							<p><strong>De ce suma donata este diferita fara de % afisat / calculat ?</strong></p>
							<p>Uneori magazinele modifica procentele pe care le avem afisate pe site, cu ocazia unor promotii / reduceri / evenimente speciale. De asemenea, procentele afisate pe site sunt aplicate la pretul fara TVA.</p>
							<div class="clearfix">&nbsp;</div>
							
						</div>
					</div>

				</div>
				<!-- about-me end -->	
			</div>	
		</section>
		<!-- main content area end  -->
		<!-- footer-area start -->
		<?php include('assets/footer.php'); ?>
			<!-- footer-top start -->
			<div class="footer-top">
				<div class="container">
					<div class="row">
						<div class="col-xs-12 col-sm-3 col-md-2 col-lg-2">
							<!-- single-footer start -->
							<div class="single-footer">
								<h2>My Account</h2>
								<ul>
									<li><a href="my-account.html">My Account</a></li>
									<li><a href="#">Log in</a></li>
									<li><a href="cart.html">My Cart</a></li>
									<li><a href="wishlist.html">My Wishlist</a></li>
									<li><a href="checkout.html">Checkout</a></li>
									<li><a href="#">Userinfor</a></li>
								</ul>
							</div>
							<!-- single-footer end -->
						</div>
						<div class="col-xs-12 col-sm-3 col-md-2 col-lg-2">
							<!-- single-footer start -->
							<div class="single-footer">
								<h2>Support</h2>
								<ul>
									<li><a href="about-us.html">About Us</a></li>
									<li><a href="#">Careers</a></li>
									<li><a href="#">Delivery</a></li>
									<li><a href="#">Privacy Policy</a></li>
									<li><a href="#">Careers</a></li>
									<li><a href="#">Sale products</a></li>
								</ul>
							</div>
							<!-- single-footer end -->
						</div>
						<div class="col-xs-12 col-sm-4 col-md-2 col-lg-2 hidden-sm hidden-xs">
							<!-- single-footer start -->
							<div class="single-footer">
								<h2>Information</h2>
								<ul>
									<li><a href="#">Sitemap</a></li>
									<li><a href="#">Privacy Policy</a></li>
									<li><a href="#">Your Account</a></li>
									<li><a href="#">Advanced</a></li>
									<li><a href="#">Privacy Policy</a></li>
									<li><a href="contact-us.html">Contact Us</a></li>
								</ul>
							</div>
							<!-- single-footer end -->
						</div>
						<div class="col-xs-12 col-sm-6 col-md-2 col-lg-2 hidden-sm hidden-xs">
							<!-- single-footer start -->
							<div class="single-footer">
								<h2>Customer</h2>
								<ul>
									<li><a href="#">roduct Recall</a></li>
									<li><a href="#">Gift Vouchers</a></li>
									<li><a href="#">Exchanges</a></li>
									<li><a href="#">Shipping</a></li>
									<li><a href="#">Gift Vouchers</a></li>
									<li><a href="#">Help & FAQs</a></li>
								</ul>
							</div>
							<!-- single-footer end -->
						</div>
						<div class="col-xs-12 col-sm-6 col-md-4 col-lg-4">
							<!-- single-footer start -->
							<div class="single-footer">
								<h2>Contact Us</h2>
								<div class="address-info">
									<p><strong>Address: </strong> <span> 123 Main Street, Anytown, CA 12345 USA.</span></p>
									<p><strong>Phone: </strong> <span> 8801973 - 833 508</span></p>
									<p><strong>Fax: </strong> <span> (800) 123 456 789</span></p>
									<p><strong>Email:  </strong> <span> <a href="mailto:admin@bootexperts.com">admin@bootexperts.com</a></span></p>
								</div>
								<div class="banner-footer">
									<img src="img/add/menuadd.jpg" alt="GreenTech" />
								</div>
							</div>
							<!-- single-footer end -->
						</div>
					</div>
				</div>
			</div>
			<!-- footer-top end -->
			<!-- footer middle start -->
			<div class="footer-middle">
				<div class="container">
					<div class="t-b-border">
						<div class="row">
							<div class="col-xs-12 col-sm-6 col-md-4">
								<div class="newsletter-box">
									<form class="form-inline" method="post" action="#">
									  <div class="form-group news-form-group">
										<h2>Newsletter</h2>
										<input type="text" class="form-control news-form-con">
										<button class="news-btn" type="submit"><i class="fa fa-angle-right"></i></button>
									  </div>
									</form>								
								</div>							
							</div>
							<div class="col-xs-12 col-sm-6 col-md-4">
								<div class="social-share">
									<div class="social-icon">
										<h2>follow</h2>
										<ul>
											<li><a class="tw" href="#"><i class="fa fa-twitter"></i></a></li>
											<li><a class="gp" href="#"><i class="fa fa-google-plus"></i></a></li>
											<li><a class="fb" href="#"><i class="fa fa-facebook"></i></a></li>
											<li><a class="li" href="#"><i class="fa fa-linkedin"></i></a></li>
											<li><a class="yu" href="#"><i class="fa fa-youtube"></i></a></li>
											<li><a class="be" href="#"><i class="fa fa-behance"></i></a></li>
										</ul>
									</div>
								</div>
							</div>
							<div class="col-xs-12 col-sm-12 col-md-4">
								<!-- payment-logo start -->
								<div class="payment-logo">
									<img src="img/payment_1.png" alt="GreenTech Payment" />
								</div>
								<!-- payment-logo end -->
							</div>								
						</div>
					</div>
				</div>
			</div>
			<!-- footer middle end -->
			<!-- footer bottom start -->
			<div class="footer-bottom">
				<div class="container">
					<div class="row">
						<div class="col-xs-12">
							<div class="copy-right-area">
								<div class="store-select">
									<label>Select Store:</label>
									<select>
										<option value="">Digital</option>
										<option value="">Furniture</option>
										<option value="">GreenTech</option>
									</select>
								</div>							
								<p class="copy-right">&copy; Copyright 2015 <a href="http://www.bootexperts.com/" target="_blank">BootExperts</a>. All Rights Reserved</p>
							</div>
							<!-- scroll-to-top-start -->
							<div class="scroll-to-top">
								<a href="#" class="greentech-scrollertop">scroll</a>
							</div>	
							<!-- scroll-to-top-end -->							
						</div>
					</div>
				</div>
			</div>
			<!-- footer bottom end -->
		</footer>
		<!-- footer-area end -->
		
		<!-- jquery js -->
		<script src="js/vendor/jquery-1.11.3.min.js"></script>
		<!-- jqueryui js -->
		<script src="js/jqueryui.js"></script>
		<!-- mobile menu js -->
		<script src="js/jquery.meanmenu.js"></script>		
		<!-- fancybox js -->
		<script src="js/jquery.fancybox.js"></script>
		<!-- elevatezoom js -->
		<script src="js/jquery.elevatezoom.js"></script>		
		<!-- bootstrap js -->
		<script src="js/bootstrap.min.js"></script>
		<!-- owl.carousel js -->
		<script src="js/owl.carousel.min.js"></script>
		<!-- owl.carousel js -->
		<script src="js/jquery.nivo.slider.pack.js"></script>
		<!-- jquery-counterup js -->
        <script src="js/jquery.counterup.min.js"></script>		
		<!-- wow js -->
        <script src="js/wow.js"></script>		
		<script>
			new WOW().init();
		</script>	
		<!-- main js -->
		<script src="js/main.js"></script>
	</body>
</html>