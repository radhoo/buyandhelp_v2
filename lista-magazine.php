<!doctype html>
<!--[if IE 9]> <html class="no-js ie9" lang="en"> <![endif]-->
<!--[if gt IE 9]><!--> <html class="no-js " lang="en"> <!--<![endif]-->
	<head>
		<!-- basic page needs -->	
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title>BuyAndHelp</title>
        <meta name="description" content="add your site description here">
		<!-- mobile meta -->
        <meta name="viewport" content="width=device-width, initial-scale=1">
		<!-- favicon -->
		<link rel="shortcut icon" type="image/x-icon" href="img/favicon.ico">
		<!-- fancybox -->
		<link rel="stylesheet" href="css/jquery.fancybox.css">
		<!-- mobile menu -->
		<link rel="stylesheet" href="css/meanmenu.min.css">		
		<!-- jquery-ui-slider -->
		<link rel="stylesheet" href="css/jquery-ui-slider.css">		
		<!-- nivo-slider css -->
		<link rel="stylesheet" href="css/nivo-slider.css">
		<!-- owl.carousel css -->
		<link rel="stylesheet" href="css/owl.transitions.css">
		<link rel="stylesheet" href="css/owl.theme.css">
		<link rel="stylesheet" href="css/owl.carousel.css">
		<!-- animate css -->
		<link rel="stylesheet" href="css/animate.css">
		<!-- fonts -->
		<link href='http://fonts.googleapis.com/css?family=Poppins:200,300,400,500,600,700,800,900' rel='stylesheet' type='text/css' />
		<!-- font-awesome css -->
		<link rel="stylesheet" href="css/font-awesome.min.css">
		<!-- bootstrap css -->
		<link rel="stylesheet" href="css/bootstrap.min.css">
		<!-- style css -->
		<link rel="stylesheet" href="style.css">
		<link rel="stylesheet" href="custom.css">
		<!-- responsive css -->
		<link rel="stylesheet" href="css/responsive.css">
		<!-- modernizr js -->
		<script src="js/vendor/modernizr-2.6.2.min.js"></script>
		<!--[if lt IE 9]>
		  <script src="js/vendor/html5shiv.min.js"></script>
		  <script src="js/vendor/respond.min.js"></script>
		<![endif]-->		
	</head>
	<body class="index-4">
		<!--[if lt IE 8]>
			<p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
		<![endif]-->
		
		<!-- Add your site or application content here -->
		<!-- header-area start -->
		<?php include('assets/header.php'); ?>
		<!-- header-area end -->
		<!-- main content area start  -->
		<section class="main-content-area">
			<div class="container">		
				<div class="row">
					<div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
						<div class="page-sidebar-area">
							<!-- shop-by-area start -->
							<div class="single-sidebar shop-by-area">
								<!-- h2>Shop By</h2 -->

								<!-- category-list-pro start -->
								<div class="category-list-pro sidebar-list">
									<h3>Categorii de produse</h3>
									<ul>
										<?php include("lib/getcategory.php"); ?>
									</ul>
								</div>
								<!-- category-list-pro end -->						
							</div>
							<!-- shop-by-area end -->
							
							<!-- single-img-add start -->
							<div class="single-img-add single-sidebar">
								<a href="#"><img src="img/add/banner33.jpg" alt="banner33" /></a>
							</div>
							<!-- single-img-add end -->
							
						</div>
					</div>
					<div class="col-xs-12 col-sm-12 col-md-9 col-lg-9">
						<div class="right-main-product">
							<!-- category-page-add start -->
							<div class="category-page-add">
								<div class="category-image-add">
									<a href="#"><img alt="image add" src="img/add/category-image.jpg"></a>
								</div>								
							</div>
							<!-- category-page-add end -->
							<!-- all-product start -->
							<div class="row all-grid-product">
								<!-- single-product-item start -->
								<?php include("lib/getstore.php"); ?>		
							</div>
							<!-- all-product end -->							
						</div>
					</div>
				</div>	
			</div>	
		</section>
		<!-- main content area end  -->
		<!-- footer-area start -->
		<?php include('assets/footer.php'); ?>
		<!-- footer-area end -->
		
		<!-- jquery js -->
		<script src="js/vendor/jquery-1.11.3.min.js"></script>
		<!-- jqueryui js -->
		<script src="js/jqueryui.js"></script>
		<!-- mobile menu js -->
		<script src="js/jquery.meanmenu.js"></script>		
		<!-- fancybox js -->
		<script src="js/jquery.fancybox.js"></script>
		<!-- elevatezoom js -->
		<script src="js/jquery.elevatezoom.js"></script>		
		<!-- bootstrap js -->
		<script src="js/bootstrap.min.js"></script>
		<!-- owl.carousel js -->
		<script src="js/owl.carousel.min.js"></script>
		<!-- owl.carousel js -->
		<script src="js/jquery.nivo.slider.pack.js"></script>
		<!-- jquery-counterup js -->
        <script src="js/jquery.counterup.min.js"></script>		
		<!-- wow js -->
        <script src="js/wow.js"></script>		
		<script>
			new WOW().init();
		</script>	
		<!-- main js -->
		<script src="js/main.js"></script>
	</body>
</html>