<!doctype html>
<!--[if IE 9]> <html class="no-js ie9" lang="en"> <![endif]-->
<!--[if gt IE 9]><!--> <html class="no-js " lang="en"> <!--<![endif]-->
	<head>
		<!-- basic page needs -->	
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title>BuyAndHelp</title>
        <meta name="description" content="add your site description here">
		<!-- mobile meta -->
        <meta name="viewport" content="width=device-width, initial-scale=1">
		<!-- favicon -->
		<link rel="shortcut icon" type="image/x-icon" href="img/favicon.ico">
		<!-- fancybox -->
		<link rel="stylesheet" href="css/jquery.fancybox.css">
		<!-- mobile menu -->
		<link rel="stylesheet" href="css/meanmenu.min.css">		
		<!-- jquery-ui-slider -->
		<link rel="stylesheet" href="css/jquery-ui-slider.css">		
		<!-- nivo-slider css -->
		<link rel="stylesheet" href="css/nivo-slider.css">
		<!-- owl.carousel css -->
		<link rel="stylesheet" href="css/owl.transitions.css">
		<link rel="stylesheet" href="css/owl.theme.css">
		<link rel="stylesheet" href="css/owl.carousel.css">
		<!-- animate css -->
		<link rel="stylesheet" href="css/animate.css">
		<!-- fonts -->
		<link href='http://fonts.googleapis.com/css?family=Poppins:200,300,400,500,600,700,800,900' rel='stylesheet' type='text/css' />
		<!-- font-awesome css -->
		<link rel="stylesheet" href="css/font-awesome.min.css">
		<!-- bootstrap css -->
		<link rel="stylesheet" href="css/bootstrap.min.css">
		<!-- style css -->
		<link rel="stylesheet" href="style.css">
		<link rel="stylesheet" href="custom.css">
		<!-- responsive css -->
		<link rel="stylesheet" href="css/responsive.css">
		<!-- modernizr js -->
		<script src="js/vendor/modernizr-2.6.2.min.js"></script>
		<!--[if lt IE 9]>
		  <script src="js/vendor/html5shiv.min.js"></script>
		  <script src="js/vendor/respond.min.js"></script>
		<![endif]-->		
	</head>
	<body class="index-4">
		<!--[if lt IE 8]>
			<p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
		<![endif]-->
		
		<!-- Add your site or application content here -->
		
		<div class="box-width-wrapper">
			<!-- header-area start -->
			<?php include('assets/header.php'); ?>
			<!-- header-area end -->
			<!-- slider-area start -->
			<div class="slider-area">
				<div class="container">
					<div class="row">
						<!-- slider start -->
						<div class="col-xs-12 col-sm-12 col-md-9">
							<div class="slider">
								<div id="mainSlider" class="nivoSlider">
									<a href="//event.2performant.com/events/click?ad_type=banner&unique=de4990a1f&aff_code=02b31f096&campaign_unique=9a7c9ac43" target="_blank">
										<img src="img/slider/home4/slider41.jpg" alt="main slider" title="#htmlcaption1"/>
									</a>
									<a href="//event.2performant.com/events/click?ad_type=banner&unique=8c78e310b&aff_code=02b31f096&campaign_unique=d4f678b43" target="_blank">
										<img src="img/slider/home4/slider42.jpg" alt="main slider" title="#htmlcaption2"/>
									</a>
								</div>	
							</div>
						</div>
						<!-- slider end -->
						<!-- block-img-add start -->
						<div class="col-xs-12 col-sm-12 col-md-3">
							<div class="block-img-add-2">
								<div class="single-image-add">
									<a href="//event.2performant.com/events/click?ad_type=quicklink&aff_code=02b31f096&unique=184f69294&redirect_to=http%253A//www.elefant.ro/carte%253Fsort%253Dsales%253Adesc" target="_blank"><img src="img/add/banner41.jpg" alt="GreenTech" /></a>
								</div>
								<div class="single-image-add">
									<a href="//event.2performant.com/events/click?ad_type=quicklink&aff_code=02b31f096&unique=5e008e4e0&redirect_to=https%253A//www.zoot.ro/categorie/22911/femei/rochii" target="_blank"><img src="img/add/banner42.jpg" alt="GreenTech" /></a>
								</div>
							</div>
						</div>
						<!-- block-img-add end -->							
					</div>
				</div>
			</div>
			<!-- slider-area end -->
			<section class="top-maincontent">
				<div class="container">
					<div class="row">
						<div class="col-xs-12 col-sm-12  col-md-9">
							<!-- deal of day product area start -->
							<div class="block2 endofday-product-area">
								<div class="section-heading">
									<h2><span>Deal</span> of the moment</h2>
								</div>
								<!-- deal-of-day-product start -->
								<div class="deal-of-day-product-h3">
									<!-- single product item start -->										
									<div class="block2-single-item">
										<div class="product-border">
											<div class="block2-pro-img">
												<a href="http://l.profitshare.ro/l/3941879" title="Fusce aliquam"><img src="img/product/home4/1.jpg" alt="product image" /></a>
											</div>
											<div class="block2-pro-text">
												<h2><a class="product-title" href="http://l.profitshare.ro/l/3941879" title="Fusce aliquam">Anvelope de iarna</a></h2>
												<div class="rating-box">
													<a title="1 star" class="rated" href="#"><i class="fa fa-star-o"></i></a>
													<a title="2 star" class="rated" href="#"><i class="fa fa-star-o"></i></a>
													<a title="3 star" class="rated" href="#"><i class="fa fa-star-o"></i></a>
													<a title="4 star" class="rated" href="#"><i class="fa fa-star-o"></i></a>
													<a title="5 star" class="rated" href="#"><i class="fa fa-star-o"></i></a>
												</div>
												<div class="product-price">
													<span class="regular-price"><a href="http://l.profitshare.ro/l/3941874" target="_blank">195 / 65 / 15</a></span>
													<span class="regular-price"> | </span>
													<span class="regular-price"><a href="http://l.profitshare.ro/l/3941876" target="_blank">205 / 55 / 16</a></span>
												</div>
												<div class="product-description">
													<p>Nu pleca la drum in sezonul rece fara anvelope de iarna. Acestea sunt obligatorii prin lege de la data de 30 octombrie. In plus te vor scapa de o amenda si te vor ajuta sa ajungi in siguranta acolo unde vrei sa mergi.</p>
												</div>	
												<div class="box-timer">
													<div class="timer">
														<div data-countdown="2017/12/24"></div>
													</div>									
												</div>														
											</div>	
										</div>
									</div>
									<!-- single product item end -->
								</div>
								<!-- deal-of-day-product end -->
							</div>
							<!-- deal of day product area end -->	

					
						</div>
						<div class="col-xs-12 col-sm-12 col-md-3">
							<div class="sidebar-area">
								<!-- featured-product start -->
								<div class="block4">
									<div class="section-heading">
										<h2><span>Donatii</span> Recente</h2>
									</div>
									<div class="row">
										<div class="featured-product">
											<!-- single carousel item start -->
											<div class="item">
												<!-- single product item start -->
												<?php include('lib/getDonationHome.php'); ?>
												<!-- single product item end -->										
											</div>
											<!-- single carousel item end -->
											
											
									</div>	
								</div>
								<!-- featured-product end -->
							</div>
						</div>						
					</div>
				</div>
			</section>
			<!-- Shop by category area start -->
			<section class="shop-by-category">
				<div class="container">
					<div class="section-heading">
						<h2><span>Subcategorii</span> de produse</h2>
					</div>				
					<div class="row">
						<!-- single-category-box start -->
						<div class="col-xs-12 col-sm-6 col-md-4">
							<div class="single-category-box">
								<div class="shop-category-item">
									<h2><a href="#">eMag</a></h2>
									<ul>
										<li><a href="http://l.profitshare.ro/l/3941639" target="_blank">Anvelope Auto</a></li>
										<li><a href="http://l.profitshare.ro/l/3941642" target="_blank">Gaming</a></li>
										<li><a href="http://l.profitshare.ro/l/3941645" target="_blank">Globuri de Craciun</a></li>
										<li><a href="http://l.profitshare.ro/l/3941647" target="_blank">Resigilate</a></li>
										<li><a href="http://l.profitshare.ro/l/3941650" target="_blank">Igiena dentara</a></li>
										<li><a href="http://l.profitshare.ro/l/3941652" target="_blank">Jucarii bebelusi</a></li>
									</ul>
								</div>
								<div class="shop-category-image">
									<img src="img/category/home4/1.jpg" alt="GreenTech" />
								</div>
							</div>
						</div>
						<!-- single-category-box end -->
						<!-- single-category-box start -->
						<div class="col-xs-12 col-sm-6 col-md-4">
							<div class="single-category-box">
								<div class="shop-category-item">
									<h2><a href="#">Fashion Days</a></h2>
									<ul>
										<li><a href="http://l.profitshare.ro/l/3941655" target="_blank">Incaltaminte femei</a></li>
										<li><a href="http://l.profitshare.ro/l/3941656" target="_blank">Incaltaminte berbati</a></li>
										<li><a href="http://l.profitshare.ro/l/3941657" target="_blank">Accesorii femei</a></li>
										<li><a href="http://l.profitshare.ro/l/3941658" target="_blank">Accesorii barbati</a></li>
										<li><a href="http://l.profitshare.ro/l/3941659" target="_blank">Haine femei</a></li>
										<li><a href="http://l.profitshare.ro/l/3941660" target="_blank">Haine barbati</a></li>
									</ul>
								</div>
								<div class="shop-category-image">
									<img src="img/category/home4/2.jpg" alt="GreenTech" />
								</div>
							</div>
						</div>
						<!-- single-category-box end -->
						<!-- single-category-box start -->					
						<div class="col-xs-12 col-sm-6 col-md-4">
							<div class="single-category-box">
								<div class="shop-category-item">
									<h2><a href="#">Lensa</a></h2>
									<ul>
										<li><a href="//event.2performant.com/events/click?ad_type=quicklink&aff_code=02b31f096&unique=444575266&redirect_to=https%253A//lensa.ro/ochelari-soare/" target="_blank">Ochelari de soare</a></li>
										<li><a href="//event.2performant.com/events/click?ad_type=quicklink&aff_code=02b31f096&unique=444575266&redirect_to=https%253A//lensa.ro/ochelari-sport/" target="_blank">Ochelari sport</a></li>
										<li><a href="//event.2performant.com/events/click?ad_type=quicklink&aff_code=02b31f096&unique=444575266&redirect_to=https%253A//lensa.ro/ochelari-sport/" target="_blank">Lentile de contact</a></li>
										<li><a href="//event.2performant.com/events/click?ad_type=quicklink&aff_code=02b31f096&unique=444575266&redirect_to=https%253A//lensa.ro/lentile-de-contact/" target="_blank">Ochelari de vedere</a></li>
										<li><a href=//event.2performant.com/events/click?ad_type=quicklink&aff_code=02b31f096&unique=444575266&redirect_to=https%253A//lensa.ro/%253Fsn.q%253Dgunnar%252520ochelari%252520protectie%252520pc%252520gunnar" target="_blank">Ochelari protectie PC</a></li>
										<li><a href="//event.2performant.com/events/click?ad_type=quicklink&aff_code=02b31f096&unique=444575266&redirect_to=https%253A//lensa.ro/%253Fsn.q%253Dgunnar%252520ochelari%252520protectie%252520pc%252520gunnar" target="_blank">Solutii si accesorii</a></li>
									</ul>
								</div>
								<div class="shop-category-image">
									<img src="img/category/home4/3.jpg" alt="GreenTech" />
								</div>
							</div>
						</div>
						<!-- single-category-box end -->			
					</div>
				</div>
			</section>
			<!-- Shop by category area end -->
			<!-- latestpost & about area start -->
			<section class="latest-about">
				<div class="container">
					<div class="row">
						<!-- latest-post-area start -->
						<div class="col-xs-12 col-sm-12 col-md-6">
							<div class="latestpost-sec-heading section-heading">
								<h2><span>articole</span> recente</h2>
							</div>					
							<div class="latest-post-area">
								<!-- single post start -->
								<div class="single-l-post">
									<div class="l-post-img">
										<a href="single-blog.html"><img src="img/post/blog11_1.jpg" alt="GreenTech" /></a>
									</div>
									<div class="l-post-text">
										<div class="post-info">
											<div class="post-time">
												<p>10 Jan 2015 <span>/</span></p>
											</div>
											<div class="posted-by">
												<p>post by <a href="#">BootExperts</a></p>
											</div>
										</div>
										<div class="post-description">
											<h2><a href="single-blog.html">green tech digital store</a></h2>
											<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod...</p>
											<a href="single-blog.html" class="readmore">citeste mai mult</a>
										</div>
									</div>
								</div>
								<!-- single post end -->
							</div>
						</div>
						<!-- latest-post-area end -->
						<!-- about-us-area start -->
						<div class="col-xs-12 col-sm-12 col-md-6">
							<div class="about-sec-heading section-heading">
								<h2><span>Despre</span> buyandhelp</h2>
							</div>						
							<div class="about-us-area">
								<div class="left-content">
									<p>BuyAndHelp.ro este un website care ajuta la finantarea cazurilor sociale din Romania, folosindu-se de comertul online si marketingul afiliat. Prin parteneriatele pe care le avem cu magazinele online, te putem ajuta sa donezi bani unor cauze sociale fara platesti nimic in plus.</p>
								</div>
								<div class="right-content">
									<img src="img/about-us/about.jpg" alt="GreenTech" />
								</div>
							</div>
						</div>
						<!-- about-us-area end -->
					</div>
				</div>
			</section>
			<!-- latestpost & about area end -->
			<!-- brand & client area start -->
			<div class="brand-client-area">
				<div class="container">
					<div class="row">
						<div class="carousel-border">
							<div class="client-carousel">
								<!-- single-client start -->
								<div class="single-client">
									<a href="" target="_blank">
										<img src="img/partners/m-dd.png" alt="GreenTech" />
									</a>	
								</div>
								<!-- single-client end -->
								<!-- single-client start -->
								<div class="single-client">
									<a href="" target="_blank">
										<img src="img/partners/gxgia.png" alt="GreenTech" />
									</a>
								</div>
								<!-- single-client end -->
								<!-- single-client start -->
								<div class="single-client">
									<a href="" target="_blank">
										<img src="img/partners/fotocolaj.png" alt="GreenTech" />
									</a>
								</div>
								<!-- single-client end -->
								<!-- single-client start -->
								<div class="single-client">
									<a href="" target="_blank">
										<img src="img/partners/captain-bean.png" alt="GreenTech" />
									</a>
								</div>
								<!-- single-client end -->
								<!-- single-client start -->
								<div class="single-client">
									<a href="" target="_blank">
										<img src="img/partners/2parale.png" alt="GreenTech" />
									</a>
								</div>
								<!-- single-client end -->
								<!-- single-client start -->
								<div class="single-client">
									<a href="" target="_blank">
										<img src="img/partners/cost-benzina.png" alt="GreenTech" />
									</a>
								</div>						
						</div>
						</div>
					</div>
				</div>
			</div>
			<!-- brand & client area end -->
			<!-- footer-area start -->
			<?php include('assets/footer.php'); ?>
			<!-- footer-area end -->
		</div>
		<!-- jquery js -->
		<script src="js/vendor/jquery-1.11.3.min.js"></script>
		<!-- jqueryui js -->
		<script src="js/jqueryui.js"></script>
		<!-- mobile menu js -->
		<script src="js/jquery.meanmenu.js"></script>		
		<!-- fancybox js -->
		<script src="js/jquery.fancybox.js"></script>
		<!-- elevatezoom js -->
		<script src="js/jquery.elevatezoom.js"></script>		
		<!-- bootstrap js -->
		<script src="js/bootstrap.min.js"></script>
		<!-- owl.carousel js -->
		<script src="js/owl.carousel.min.js"></script>
		<!-- owl.carousel js -->
		<script src="js/jquery.nivo.slider.pack.js"></script>
		<!-- jquery-counterup js -->
        <script src="js/jquery.counterup.min.js"></script>		
		<!-- wow js -->
        <script src="js/wow.js"></script>		
		<script>
			new WOW().init();
		</script>		
		<!-- main js -->
		<script src="js/main.js"></script>
	</body>
</html>