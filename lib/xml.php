<?php

	include('config.php');

	/****************/
	/*  Get Stores  */
	/****************/
	$res = mysql_query('SELECT ID, Name FROM company');
	
	$xml = new XMLWriter();

	$xml->openURI("php://output");
	$xml->startDocument();
	$xml->setIndent(true);

	$xml->startElement('stores');

	while ($row = mysql_fetch_assoc($res)) {
	  $xml->startElement("store");

	  $xml->writeAttribute('ID', $row['ID']);
	  $xml->writeRaw($row['Name']);

	  $xml->endElement();
	}

	$xml->endElement();

	header('Content-type: text/xml');
	$xml->flush();
	
	
?>