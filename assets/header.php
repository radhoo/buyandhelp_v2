<header class="header-area">
	<!-- header-top start -->
	<div class="header-top">
		<div class="container">
			<div class="row">								
						<div class="col-md-2 infoStats">Procent maxim<br/> <span><?php include('lib/procent.php');  ?>%</span></div>
						<div class="col-md-2 infoStats">ONG-uri partenere<br/> <span><?php include('lib/countCause.php'); ?></span></div>
						<div class="col-md-2 infoStats">Transferuri catre ONG-uri<br/> <span><?php include('lib/transferuri.php'); ?></span></div>
						<div class="col-md-2 infoStats">Cea mai mare donatie<br/> <span>201 lei</span></div>
						<div class="col-md-2 infoStats">Numar total donatii<br/> <span><?php include('lib/donatii.php'); ?></span></div>
						<div class="col-md-2 infoStats">Vanzari totale generate<br/> <span><?php include('lib/vanzari.php'); ?> Lei</span></div>
			</div>
		</div>
	</div>
	<!-- header-top end -->
	<!-- header-middle start -->			
	<div class="header-middle">
		<div class="container">
			<div class="row">
				<div class="col-xs-12 col-md-3">
					<!-- logo start -->
					<div class="logo">
						<a href="/"><img src="img/logo.png" alt="GreenTech" /></a>
					</div>
					<!-- logo end -->
				</div>
				<div class="col-xs-12 col-md-9">
					<!-- category search start -->
					<div class="category-search-area">
						<!-- div class="search-cat">
							<select>
								<option value="">Categorii</option>
								<option value="">Accesorii</option>
								<option value="">Asigurari auto</option>
								<option value="">Cadouri</option>
								<option value="">Carti</option>
								<option value="">Copii</option>
								<option value="">Cosmetice</option>
								<option value="">Electronice</option>
								<option value="">Fashion</option>
							</select>
						</div -->
						<div class="search-form" style="display:none">
							<form action="#" method="post">
								<input class="cat-search-box" type="text" placeholder="Search entire store here.." />
								<a href="" class="cat-search-btn"><i class="fa fa-search"></i></a>
							</form>	
						</div>
					</div>
					<!-- category search end -->
					<!-- top-shoping-cart start -->
					<div class="top-shoping-cart">
					
						<div class="top-mycart hidden-xs">
							<a href="https://chrome.google.com/webstore/detail/buyandhelp/ocabjoiiblekibbmolobgcipmnjhhmap?hl=ro" target="_blank"><img src="img/chrome-extenssion.png" /></a>		
						</div>
					</div>
					<!-- top-shoping-cart end -->							
				</div>
			</div>
		</div>
	</div>
	<!-- header-middle end -->
	<!-- header-bottom start -->
	<div class="header-bottom">
		<div class="container">
			<div class="row">	
				<!-- mainmenu start -->
				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
					<div class="mainmenu">
						<nav>
							<ul>
								<li><a href="/">Home</a></li>	
								<li><a href="despre-buyandhelp.php">Despre BuyAndHelp.ro</a></li>
								<li><a href="lista-magazine.php">Lista magazine</a></li>
								<li><a href="cauze.php">Cauze sociale</a></li>
								<li><a href="calculator.php">Calculator donatie</a></li>
								<li><a href="media.php">In media</a></li>
								<li><a href=".php">Blog</a></li>
								<li><a href="faq.php">FAQ</a></li>
								<li><a href="contact.php">Contact</a></li>
							</ul>
						</nav>
					</div>
				</div>
				<!-- mainmenu end -->
			</div>
			<!-- mobile menu start -->
			<div class="row">
				<div class="col-sm-12 mobile-menu-area">
					<div class="mobile-menu hidden-md hidden-lg" id="mob-menu">
						<span class="mobile-menu-title">Menu</span>
						<nav>
							<ul>
								<li><a href="/">Home</a></li>	
								<li><a href="despre-buyandhelp.php">Despre BuyAndHelp.ro</a></li>
								<li><a href="lista-magazine.php">Lista magazine</a></li>
								<li><a href="cauze.php">Cauze sociale</a></li>
								<li><a href="calculator.php">Calculator donatie</a></li>
								<li><a href="media.php">In media</a></li>
								<li><a href=".php">Blog</a></li>
								<li><a href="faq.php">FAQ</a></li>
								<li><a href="contact.php">Contact</a></li>
							</ul>
						</nav>
					</div>						
				</div>
			</div>
			<!-- mobile menu end -->						
		</div>
	</div>
	<!-- header-bottom end -->
</header>
