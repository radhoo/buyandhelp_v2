<footer class="footer-area">
	<!-- footer-top start -->
	<div class="footer-top">
		<div class="container">
			<div class="row">
				<div class="col-xs-12 col-sm-3 col-md-2 col-lg-2">
					<!-- single-footer start -->
					<div class="single-footer">
						<h2>Meniu</h2>
						<ul>
							<li><a href="despre-buyandhelp.php">Despre noi</a></li>
							<li><a href="lista-magazine.php">Lista magazine</a></li>
							<li><a href="cauze.php">Cauze sociale</a></li>
							<li><a href="media.php">In media</a></li>
							<li><a href="faq.php">Intrebari frecvente</a></li>
							<li><a href="#">Blog</a></li>
						</ul>
					</div>
					<!-- single-footer end -->
				</div>
				<div class="col-xs-12 col-sm-3 col-md-2 col-lg-2">
					<!-- single-footer start -->
					<div class="single-footer">
						<!--h2>Support</h2>
						<ul>
							<li><a href="about-us.html">About Us</a></li>
							<li><a href="#">Careers</a></li>
							<li><a href="#">Delivery</a></li>
							<li><a href="#">Privacy Policy</a></li>
							<li><a href="#">Careers</a></li>
							<li><a href="#">Sale products</a></li>
						</ul-->
					</div>
					<!-- single-footer end -->
				</div>
				<div class="col-xs-12 col-sm-4 col-md-2 col-lg-2 hidden-sm hidden-xs">
					<!-- single-footer start -->
					<div class="single-footer">
						<!--h2>Information</h2>
						<ul>
							<li><a href="#">Sitemap</a></li>
							<li><a href="#">Privacy Policy</a></li>
							<li><a href="#">Your Account</a></li>
							<li><a href="#">Advanced</a></li>
							<li><a href="#">Privacy Policy</a></li>
							<li><a href="contact-us.html">Contact Us</a></li>
						</ul-->
					</div>
					<!-- single-footer end -->
				</div>
				<div class="col-xs-12 col-sm-6 col-md-2 col-lg-2 hidden-sm hidden-xs">
					<!-- single-footer start -->
					<div class="single-footer">
						<!-- h2>Customer</h2>
						<ul>
							<li><a href="#">roduct Recall</a></li>
							<li><a href="#">Gift Vouchers</a></li>
							<li><a href="#">Exchanges</a></li>
							<li><a href="#">Shipping</a></li>
							<li><a href="#">Gift Vouchers</a></li>
							<li><a href="#">Help & FAQs</a></li>
						</ul-->
					</div>
					<!-- single-footer end -->
				</div>
				<div class="col-xs-12 col-sm-6 col-md-4 col-lg-4">
					<!-- single-footer start -->
					<div class="single-footer">
						<h2>Contacteaza-ne</h2>
						<div class="address-info">
							<p><strong>Adresa de corespondenta</strong></p><br/>
							<p><span>str. Magnoliei, nr.3, ap.21, Baciu, jud. Cluj</span></p>
							<p>&nbsp;</p>
							<p><strong>Email:  </strong> <span> <a href="mailto:contact@buyandhelp.ro">contact@buyandhelp.ro</a></span></p>
						</div>
						<div class="banner-footer">
							<img src="img/add/menuadd.jpg" alt="GreenTech" />
						</div>
					</div>
					<!-- single-footer end -->
				</div>
			</div>
		</div>
	</div>
	<!-- footer-top end -->
	<!-- footer middle start -->
	<div class="footer-middle">
		<div class="container">
			<div class="t-b-border">
				<div class="row">
					<div class="col-xs-12 col-sm-7 col-md-8">
						<div class="newsletter-box">
							<form class="form-inline" method="post" action="#">
							  <div class="form-group news-form-group">
								<h2>Newsletter</h2>
								<input type="text" class="form-control news-form-con">
								<button class="news-btn" type="submit"><i class="fa fa-angle-right"></i></button>
							  </div>
							</form>								
						</div>							
					</div>
					<div class="col-xs-12 col-sm-5 col-md-4">
						<div class="social-share">
							<div class="social-icon">
								<h2>follow</h2>
								<ul>
									<li><a class="tw" href="https://twitter.com/BuyAndHelpRo"><i class="fa fa-twitter"></i></a></li>
									<li><a class="fb" href="https://twitter.com/BuyAndHelpRo"><i class="fa fa-facebook"></i></a></li>
									<li><a class="yu" href="https://www.youtube.com/watch?v=XIFTkEJ9qdc"><i class="fa fa-youtube"></i></a></li>
								</ul>
							</div>
						</div>
					</div>						
				</div>
			</div>
		</div>
	</div>
	<!-- footer middle end -->
	<!-- footer bottom start -->
	<div class="footer-bottom">
		<div class="container">
			<div class="row">
				<div class="col-xs-12">
					<div class="copy-right-area">						
						<p class="copy-right">&copy; Copyright 2014-2017. All Rights Reserved.</p>
					</div>
					<!-- scroll-to-top-start -->
					<div class="scroll-to-top">
						<a href="#" class="greentech-scrollertop">scroll</a>
					</div>	
					<!-- scroll-to-top-end -->							
				</div>
			</div>
		</div>
	</div>
	<!-- footer bottom end -->
</footer>
<!-- Start of StatCounter Code for Default Guide -->

	<script type="text/javascript">
	var sc_project=10085571; 
	var sc_invisible=1; 
	var sc_security="c0c5193d"; 
	var scJsHost = (("https:" == document.location.protocol) ?
	"https://secure." : "http://www.");
	document.write("<sc"+"ript type='text/javascript' src='" +
	scJsHost+
	"statcounter.com/counter/counter.js'></"+"script>");
	</script>

	<noscript><div class="statcounter"><a title="free web stats"
	href="http://statcounter.com/" target="_blank"><img
	class="statcounter"
	src="http://c.statcounter.com/10085571/0/c0c5193d/1/"
	alt="free web stats"></a></div></noscript>
	<!-- End of StatCounter Code for Default Guide -->
	<script>
	  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
	  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
	  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
	  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

	  ga('create', 'UA-53631382-1', 'auto');
	  ga('send', 'pageview');
	</script>
