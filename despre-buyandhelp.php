<!doctype html>
<!--[if IE 9]> <html class="no-js ie9" lang="en"> <![endif]-->
<!--[if gt IE 9]><!--> <html class="no-js " lang="en"> <!--<![endif]-->
	<head>
		<!-- basic page needs -->	
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title>BuyAndHelp</title>
        <meta name="description" content="add your site description here">
		<!-- mobile meta -->
        <meta name="viewport" content="width=device-width, initial-scale=1">
		<!-- favicon -->
		<link rel="shortcut icon" type="image/x-icon" href="img/favicon.ico">
		<!-- fancybox -->
		<link rel="stylesheet" href="css/jquery.fancybox.css">
		<!-- mobile menu -->
		<link rel="stylesheet" href="css/meanmenu.min.css">		
		<!-- jquery-ui-slider -->
		<link rel="stylesheet" href="css/jquery-ui-slider.css">		
		<!-- nivo-slider css -->
		<link rel="stylesheet" href="css/nivo-slider.css">
		<!-- owl.carousel css -->
		<link rel="stylesheet" href="css/owl.transitions.css">
		<link rel="stylesheet" href="css/owl.theme.css">
		<link rel="stylesheet" href="css/owl.carousel.css">
		<!-- animate css -->
		<link rel="stylesheet" href="css/animate.css">
		<!-- fonts -->
		<link href='http://fonts.googleapis.com/css?family=Poppins:200,300,400,500,600,700,800,900' rel='stylesheet' type='text/css' />
		<!-- font-awesome css -->
		<link rel="stylesheet" href="css/font-awesome.min.css">
		<!-- bootstrap css -->
		<link rel="stylesheet" href="css/bootstrap.min.css">
		<!-- style css -->
		<link rel="stylesheet" href="style.css">
		<link rel="stylesheet" href="custom.css">
		<!-- responsive css -->
		<link rel="stylesheet" href="css/responsive.css">
		<!-- modernizr js -->
		<script src="js/vendor/modernizr-2.6.2.min.js"></script>
		<!--[if lt IE 9]>
		  <script src="js/vendor/html5shiv.min.js"></script>
		  <script src="js/vendor/respond.min.js"></script>
		<![endif]-->		
	</head>
	<body class="index-4">
		<!--[if lt IE 8]>
			<p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
		<![endif]-->
		
		<!-- Add your site or application content here -->
		<!-- header-area start -->
		<?php include('assets/header.php'); ?>
		<!-- header-area end -->
		<!-- main content area start  -->
		<section class="main-content-area">
			<div class="container">		
				<!-- about-me start -->
				<div class="row about-me">
					<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
						<div class="about-greentech-text">
							<h1>Despre <strong>BuyAndHelp</strong></h1>
							<p><strong>BuyAndHelp.ro</strong> este un website care ajuta la finantarea cazurilor sociale din Romania, folosindu-se de comertul online si marketingul afiliat. Prin parteneriatele pe care le avem cu magazinele online, te putem ajuta sa donezi bani unor cauze sociale fara platesti nimic in plus. </p>
							<p>Conform datelor oferite de <a href="http://www.gpec.ro/" target="_blank">GPEC</a>, in 2013 numarul utilizatorilor de Internet a ajuns la 10 milioane. 27% dintre ei cumpara produse online in valoare de aproximativ 600 milioane de dolari.</p>
							<p>Prin intermediul marketingului afiliat, magazinele online platesc comisioane din fiecare vanzare celor care le intermediaza. BuyAndHelp.ro intermediaza achizitia produselor online, primind astfel comisioane care vor fi donate cauzelor sociale alese de utilizatori. Putem explica cel mai bine tot procesul printr-un exemplu foarte simplu. Sa spunem ca tu doresti sa iti cumperi o tableta care costa 1000 de lei, de pe website-ul unui magazin online (sa il numim X). Intrand pe site-ul magazinului X prin intermediul BuyAndHelp.ro (click pe logoul acestuia) noi vom primi un comision procentual (sa presupunem ca acesta este 2%). Dupa ce vei cumpara tableta, magazinul X va transfera catre BuyAndHelp.ro comisionul de 2% adica 20 de lei, iar noi il vom dona catre cauza sociala pe care vrei sa o sustii. <b>Toate acestea fara ca tu sa scoti mai multi bani din buzunar.</b></p>
							<p>Proiectul BuyAndHelp.ro a fost initial de Radu Popescu, un tanar clujean implicat in actiuni cartabile din anul 2007 prin campania sa de ajutorare a copiilor din familii sarace "un Craciun mai bun".</p>			
						</div>
					</div>
					<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
						<div class="about-greentech-img">
							<img src="img/about-us/team30.jpg" alt="" />
						</div>					
					</div>
				</div>
				<!-- about-me end -->
				<!-- creative-member-area end -->
				<div class="row creative-member-area">	
					<div class="about-sec-head">
						<h2 class="creative-member">Echipa</h2>
						<p>Echipa de baza care se ocupa de acest proiect este formata din doua persoane:</p>
					</div>
					<div class="col-xs-12 col-sm-6 col-md-3 col-lg-3 col-md-offset-3">
						<div class="single-creative-member">
							<div class="member-image">
								<img src="img/about-us/radu-popescu.jpg" alt="" />
								<div class="member-title">
									<h2>Radu Popescu</h2>
									<h3>Tech Co-Founder</h3>									
								</div>
							</div>
							<div class="member-info">
								<p>Peste 11 ani de experienta in IT-ul clujean.</p>
								<div class="member-social">
									<a class="m-facebook" href="https://www.facebook.com/ppsradu"><i class="fa fa-facebook"></i></a>
									<a class="m-twitter" href="https://twitter.com/radhoo"><i class="fa fa-twitter"></i></a>
									<a class="m-linkedin" href="https://www.linkedin.com/in/popescuradu/"><i class="fa fa-linkedin"></i></a>
								</div>
							</div>
						</div>
					</div>
					<div class="col-xs-12 col-sm-6 col-md-3 col-lg-3">
						<div class="single-creative-member">
							<div class="member-image">
								<img src="img/about-us/angela-lepadatu.jpg" alt="" />
								<div class="member-title">
									<h2>Angela Lepadatu</h2>
									<h3>Marketing Co-Founder</h3>									
								</div>
							</div>
							<div class="member-info">
								<p>Marketer cu experienta in zona IT Security.</p>
								<div class="member-social">
									<a class="m-facebook" href="https://www.facebook.com/profile.php?id=100000304390999"><i class="fa fa-facebook"></i></a>
									<a class="m-linkedin" href="https://www.linkedin.com/in/angela-lepadatu-39061924/"><i class="fa fa-linkedin"></i></a>
								</div>
							</div>
						</div>
					</div>
				</div>
				<!-- creative-member-area end -->			
			</div>	
		</section>
		<!-- main content area end  -->
		<!-- footer-area start -->
		<footer>
		<?php include('assets/footer.php'); ?>
		</footer>
		<!-- footer-area end -->
		
		<!-- jquery js -->
		<script src="js/vendor/jquery-1.11.3.min.js"></script>
		<!-- jqueryui js -->
		<script src="js/jqueryui.js"></script>
		<!-- mobile menu js -->
		<script src="js/jquery.meanmenu.js"></script>		
		<!-- fancybox js -->
		<script src="js/jquery.fancybox.js"></script>
		<!-- elevatezoom js -->
		<script src="js/jquery.elevatezoom.js"></script>		
		<!-- bootstrap js -->
		<script src="js/bootstrap.min.js"></script>
		<!-- owl.carousel js -->
		<script src="js/owl.carousel.min.js"></script>
		<!-- owl.carousel js -->
		<script src="js/jquery.nivo.slider.pack.js"></script>
		<!-- jquery-counterup js -->
        <script src="js/jquery.counterup.min.js"></script>		
		<!-- wow js -->
        <script src="js/wow.js"></script>		
		<script>
			new WOW().init();
		</script>	
		<!-- main js -->
		<script src="js/main.js"></script>
	</body>
</html>