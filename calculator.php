<!doctype html>
<!--[if IE 9]> <html class="no-js ie9" lang="en"> <![endif]-->
<!--[if gt IE 9]><!--> <html class="no-js " lang="en"> <!--<![endif]-->
	<head>
		<!-- basic page needs -->	
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title>BuyAndHelp</title>
        <meta name="description" content="add your site description here">
		<!-- mobile meta -->
        <meta name="viewport" content="width=device-width, initial-scale=1">
		<!-- favicon -->
		<link rel="shortcut icon" type="image/x-icon" href="img/favicon.ico">
		<!-- fancybox -->
		<link rel="stylesheet" href="css/jquery.fancybox.css">
		<!-- mobile menu -->
		<link rel="stylesheet" href="css/meanmenu.min.css">		
		<!-- jquery-ui-slider -->
		<link rel="stylesheet" href="css/jquery-ui-slider.css">		
		<!-- nivo-slider css -->
		<link rel="stylesheet" href="css/nivo-slider.css">
		<!-- owl.carousel css -->
		<link rel="stylesheet" href="css/owl.transitions.css">
		<link rel="stylesheet" href="css/owl.theme.css">
		<link rel="stylesheet" href="css/owl.carousel.css">
		<!-- animate css -->
		<link rel="stylesheet" href="css/animate.css">
		<!-- fonts -->
		<link href='http://fonts.googleapis.com/css?family=Poppins:200,300,400,500,600,700,800,900' rel='stylesheet' type='text/css' />
		<!-- font-awesome css -->
		<link rel="stylesheet" href="css/font-awesome.min.css">
		<!-- bootstrap css -->
		<link rel="stylesheet" href="css/bootstrap.min.css">
		<!-- style css -->
		<link rel="stylesheet" href="style.css">
		<link rel="stylesheet" href="custom.css">
		<!-- responsive css -->
		<link rel="stylesheet" href="css/responsive.css">
		<!-- modernizr js -->
		<script src="js/vendor/modernizr-2.6.2.min.js"></script>
		<!--[if lt IE 9]>
		  <script src="js/vendor/html5shiv.min.js"></script>
		  <script src="js/vendor/respond.min.js"></script>
		<![endif]-->		
	</head>
	<body class="index-4">
		<!--[if lt IE 8]>
			<p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
		<![endif]-->
		
		<!-- Add your site or application content here -->
		<!-- header-area start -->
		<?php include('assets/header.php'); ?>
		<!-- header-area end -->
		<!-- main content area start  -->
		
		<section class="main-content-area">
			<div class="container">
				<div class="row">
								<form role="form">
								  <div class="form-group">
									<label for="exampleInputEmail1">Adauga pretul produsului cumparat</label>
									<input type="text" class="form-control" id="pret" name="sum" placeholder="Adauga pretul ex: 749.99">
								  </div>
								  <div class="form-group">
								    <label for="exampleInputEmail1">Selecteaza magazinul</label>
									<select class="form-control" id="store" name="shop">
										<?php
											include('lib/config.php');
											
											$results = mysql_query('SELECT * FROM company ORDER BY Name ASC');
												
											while($row=mysql_fetch_array($results)){
												echo '<option value="'.$row['Percent']*0.5.'" title="'.$row['ID'].'">'.$row['Name'].'</option>';
											}
										?>
									</select>
								   </div>
									<div class="form-group">
										<div class="add-cart-button">
											<a href="javascript:void(0)" id="calculeaza" class="add-tag-btn">Calculeaza</a>
										</div>
									</div>
									<br/>
									<div id="DonateResult"></div>
								</form>
				</div>	
			</div>	
		</section>

		<div>&nbsp;</div>
		<!-- main content area end  -->
		<!-- footer-area start -->
		<?php include('assets/footer.php'); ?>
		<!-- footer-area end -->
		
		<!-- jquery js -->
		<script src="js/vendor/jquery-1.11.3.min.js"></script>
		<!-- jqueryui js -->
		<script src="js/jqueryui.js"></script>
		<!-- mobile menu js -->
		<script src="js/jquery.meanmenu.js"></script>		
		<!-- fancybox js -->
		<script src="js/jquery.fancybox.js"></script>
		<!-- elevatezoom js -->
		<script src="js/jquery.elevatezoom.js"></script>		
		<!-- bootstrap js -->
		<script src="js/bootstrap.min.js"></script>
		<!-- owl.carousel js -->
		<script src="js/owl.carousel.min.js"></script>
		<!-- owl.carousel js -->
		<script src="js/jquery.nivo.slider.pack.js"></script>
		<!-- jquery-counterup js -->
        <script src="js/jquery.counterup.min.js"></script>		
		<!-- wow js -->
        <script src="js/wow.js"></script>		
		<script>
			new WOW().init();
		</script>	
		<!-- main js -->
		<script src="js/main.js"></script>
	</body>
</html>